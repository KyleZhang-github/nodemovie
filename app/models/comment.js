var mongoose = require('mongoose')
var CommentSchema = require('../schemas/comment')

//通过mongoose的model发布comment的模型
var Comment = mongoose.model('Comment', CommentSchema)

module.exports = Comment