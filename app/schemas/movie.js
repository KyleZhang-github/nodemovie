/**
 * Created by kylezhang on 6/29/15.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema
var ObjectId = Schema.Types.ObjectId

var MovieSchema = new Schema({
  director: String,
  title: String,
  language: String,
  country: String,
  summary: String,
  flash: String,
  poster: String,
  year: Number,
  category: {
    type:ObjectId,
    ref: 'category'
  },
  meta: {
    createAt: {
        type: Date,
        default: Date.now()
    },
    updateAt: {
        type: Date,
        default: Date.now()
    }
  }
});

//保存数据用
MovieSchema.pre('save',function(next){
  if(this.isNew){
      this.meta.createAt=this.meta.updateAt=Date.now()
  }else{
      this.meta.updateAt=Date.now()
  }
  next()
})

// 定义静态方法
MovieSchema.statics={
  fetch:function(cb){
      return this.find({}).sort('meta.updateAt').exec(cb)
  },
  findById:function(id, cb){
      return this.findOne({_id:id}).exec(cb)
  }
}

module.exports=MovieSchema