/*负责和首页进行交互*/
var Movie = require('../models/movie')
var Category = require('../models/category')


// index page
exports.index = function(req, res){
    //这里请记住，这样console.log('自己挖坑：' + req.session)会把session toString的，打印的结果会不正确地
    //但是数据库中的session已经正确地存储了，也就是说 "+" String的类型权限很高
    console.log('自己挖坑：', req.session, '我来个去得')  //这是正确地，
    
    Category
      .find({})
      .populate({path: 'movies', option:{limit: 5}})
      .exec(function(err, categories){
        if(err){
            console.log(err)
        }
        res.render('index', {
            title: 'imooc 首页@zhangkai',
            categories: categories
        })
      })
        
}