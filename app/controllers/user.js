var User = require('../models/user')

// user signin page
exports.showSignin = function(req, res){
  res.render('signin', {
    title: '登陆页面'
  })
}

// user signup page
exports.showSignup = function(req, res){
  res.render('signup', {
    title: '注册页面'
  })
}

exports.signup = function(req, res){
  var _user = req.body.user

  User.findOne({name: _user.name}, function(err, user){
       if(err){
          console.log(err);
       }

       if(user){
          //console.log('Test-----:' + user)
          //console.log('用户%s已经存在，直接跳转到首页', user.name)
          return res.redirect('/signin')
       }else{
          var user = new User(_user)
          console.log('用户%s不存在，新加入到数据库：',user.name);

          user.save(function(err, user){
            if(err){
                console.log(err)
            }

            res.redirect('/')
          })
       }
  })
}

// userlist page
exports.list = function(req, res){
  User.fetch(function(err, users){
      if(err){
          console.log(err)
      }
      res.render('userlist', {
          title: 'imooc 用户列表页',
          users: users
      })
  })
}

//signin
exports.signin = function(req, res){
  var _user = req.body.user
  var name = _user.name
  var password = _user.password

  User.findOne({name: name}, function(err, user){
    if(err){
      console.log(err);
    }

    if(!user){
      return res.redirect('/signup')
    }

    user.comparePassword(password, function(err, isMatch){
      if(err){
        console.log(err);
      }
      if(isMatch){
        //console.log(req.session);
        req.session.user = user
        //console.log('登陆成功，跳转到了首页\n')
        return res.redirect('/')
        
      }else{
        return res.redirect('/signin')
      }
    })
  })
}

//logout
exports.logout = function(req, res){
  delete req.session.user
  //delete app.locals.user
  res.redirect('/')
}

// middleWare for user:确定用户是否登录
exports.signinRequired = function(req, res, next){
  var user = req.session.user

  if(!user){
    return res.redirect('/signin')
  }

  //确定了是登录了得用户，继续后续的
  next()
}

// middleWare for admin:确定用户的权限
exports.adminRequired = function(req, res, next){
  var user = req.session.user

  if(user.role <= 10){
    //非admin权限
    return res.redirect('/signin')
  }

  next()
}