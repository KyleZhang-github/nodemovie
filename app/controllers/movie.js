var Movie = require('../models/movie')
var Category = require('../models/category')
var Comment = require('../models/comment')
var _ = require('underscore')

//detail page
exports.detail = function(req, res){
  var id = req.params.id
  Movie.findById(id, function(err, movie){
      //Comment.find({movie: id}, function(err, comments){ //这是用callback的方式查询
      Comment
        .find({movie: id})
        .populate('from', 'name')
        .populate('reply.from reply.to', 'name')
        .exec(function(err, comments){
          
        console.log("在comment的controllers中打印\n");
        console.log(comments);
          if(err){
              console.log(err + 'zhangkai')
          }
          res.render('detail', {
              title: 'imooc ' + movie.title,
              movie: movie,
              comments: comments
          })
        })
  })
}

// admin new page
exports.new = function(req, res){
  Category.find({}, function(err, categories){
    res.render('admin', {
          title: 'imooc 后台管理页',
          categories: categories,
          movie: {}
    })
  })
}

// admin update movie
exports.update = function(req, res){
    var id = req.params.id

    if(id){
        Movie.findById(id, function(err, movie){
            if(err){
                console.log(err)
            }
            res.render('admin', {
                title: 'imooc 后台更新页',
                movie: movie
            })
        })
    }
}

//admin post movie
exports.save = function(req, res){
    var id=req.body._id
    var movieObj={
        _id:req.body._id,
        title:req.body.title,
        director:req.body.director,
        country:req.body.country,
        language:req.body.language,
        year:req.body.year,
        poster:req.body.poster,
        summary:req.body.summary,
        flash:req.body.flash
    }
    var _movie
    if(id){
        Movie.findById(id,function(err,movie){
            if(err){
                console.log(err)
            }

            _movie=_.extend(movie,movieObj)
            _movie.save(function(err,movie){
                if(err){
                    console.log(err)
                }
                res.redirect('/movie/' + movie._id)
            })
        })
    }
    else{
        console.log('张凯：movieObj');
        console.log(movieObj);
        _movie=new Movie(movieObj)
        _movie.save(function(err,movie){
            if(err){
                console.log(err)
            }
            console.log('张凯：', movie);
            res.redirect('/movie/' + movie._id)
        })
    }
}

// list page
exports.list = function(req, res){
    Movie.fetch(function(err, movies){
        if(err){
            console.log(err)
        }
        res.render('list', {
            title: 'imooc 列表页',
            movies: movies
        })
    })
}

// list delete movie
exports.del = function(req, res){
    console.log('test')
    var id = req.query.id

    if(id){
        Movie.remove({_id: id}, function(err, movie){
            if(err){
                console.log(err);
            }else{
                res.json({success: 1})
            }
        })
    }
}