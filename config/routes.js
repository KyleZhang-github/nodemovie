var bodyParser = require('body-parser')
var _ = require('underscore')
var Index = require('../app/controllers/index')
var User = require('../app/controllers/user')
var Movie = require('../app/controllers/movie')
var Comment = require('../app/controllers/comment')
var Category = require('../app/controllers/category')


//router main 
module.exports = function(app){

    // pre handle user
    app.use(function(req, res, next){
        
        var _user = req.session.user
        app.locals.user = _user

        return next()
    })

    // index
    app.get('/', Index.index)

    // user
    app.post('/user/signup', User.signup)
    app.post('/user/signin', User.signin)
    app.get('/signin', User.showSignin)
    app.get('/signup', User.showSignup)
    app.get('/logout', User.logout)
    app.get('/admin/userlist', User.signinRequired, User.adminRequired, User.list)

    // movie
    app.get('/movie/:id', Movie.detail)
    app.get('/admin/movie', Movie.new)
    app.get('/admin/update/:id', Movie.update)
    app.post('/admin/movie/new', Movie.save)
    app.get('/admin/list', Movie.list)
    app.delete('/admin/list', Movie.del)

    //Commet
    app.post('/user/comment', User.signinRequired, Comment.save)

    //Category
    app.get('/admin/category/new', User.signinRequired, User.adminRequired, Category.new)
    app.post('/admin/category', User.signinRequired, User.adminRequired, Category.save)
    app.get('/admin/category/list', User.signinRequired, User.adminRequired, Category.list)

}