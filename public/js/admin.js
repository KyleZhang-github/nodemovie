$(function(){
    $('.del').click(function(e){
        var target = $(e.target)
        //此处走了一个弯路，刚开始把target.data('id')=》target.data(id)，总是说.item-id-***找不到
        var id = target.data('id')
        var tr = $(".item-id-" + id)
        console.log("用户的删除");

        $.ajax({
            type: 'DELETE',
            url: '/admin/list?id=' + id
        }).done(function(results){
            if(results.success === 1){
                if(tr.length > 0){
                    tr.remove()
                }
            }
        })
    })
})