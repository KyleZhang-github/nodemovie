var express = require('express')
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')
var session = require('express-session')
var path = require('path')
var mongoose = require('mongoose')
var mongoStore = require('connect-mongo')(session)
var morgan = require('morgan')

var port = process.env.PORT || 5000
var app = express()

// 数据库的链接
var dbUrl = 'mongodb://localhost/imooc'
mongoose.connect(dbUrl)

app.set('views', './app/views/pages')
app.set('view engine', 'jade')
app.use(bodyParser.urlencoded({extended: true}))
//现在已经没有和express绑定了，改用'body-parser'
//app.use(express.bodyParser())

app.use(session({
  resave: true,
  saveUninitialized: true,
  secret: 'imooc',
  store: new mongoStore({
    url: dbUrl,
    collection: 'session'
  })
}))

//development config
if('development' === app.get('env')){
  app.set('showStacKError', true)

  //下面的logger是3.x中的中间件
  //app.use(express.logger(':method :url :status'))
  //express4.x中不在是logger，改为模块morgan;直接使用dev和上面logger的format相似
  app.use(morgan('dev'))

  app.locals.pretty = true
  mongoose.set('debug', true)
}

//use router
require('./config/routes.js')(app)

app.use(express.static(path.join(__dirname, '/public')))
app.listen(port)
app.locals.moment = require('moment')

console.log('imooc started on port ' + port)